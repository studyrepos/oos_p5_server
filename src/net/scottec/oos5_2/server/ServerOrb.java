package net.scottec.oos5_2.server;

import net.scottec.oos5_2.common.Benutzer;
import net.scottec.oos5_2.common.BenutzerDoppeltException;
import net.scottec.oos5_2.common.BenutzerVerwaltungAdmin;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

class ServerOrb implements Runnable {

    private BenutzerVerwaltungAdmin benutzerVerwaltungAdmin;

    ServerOrb(BenutzerVerwaltungAdmin _benutzerVerwaltungAdmin)  {
        this.benutzerVerwaltungAdmin = _benutzerVerwaltungAdmin;
        run();
    }

    public void run() {
        System.out.println("Remote Server gestartet...");
        try {
            ServerSocket socket = new ServerSocket(4711);

            while (true) {
                Socket client = socket.accept();
                System.out.println("Eingehende Verbindung");
                ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
                ObjectInputStream ois = new ObjectInputStream(client.getInputStream());

                try {
                    int func = ois.readInt();
                    String userid = ois.readUTF();
                    String password = ois.readUTF();

                    System.out.println("func: " + func + " - userid: " + userid + " - password: " + password);

                    switch (func) {
                        case 1:
                            if (this.benutzerVerwaltungAdmin.benutzerOk(
                                    new Benutzer(userid, password.toCharArray()))) {
                                oos.writeUTF("OK");
                                System.out.println("OK");
                            }
                            else {
                                oos.writeUTF("ERR");
                                System.out.println("ERR");
                            }
                            oos.flush();
                            break;

                        case 2:
                            this.benutzerVerwaltungAdmin.benutzerEintragen(
                                    new Benutzer(userid, password.toCharArray()));
                            oos.writeUTF("OK");
                            oos.flush();
                            break;
                    } // switch
                    client.close();
                } catch (BenutzerDoppeltException exp) {
                    oos.writeUTF("DOPPELT");
                    oos.flush();
                }
            } // while
        } catch (IOException exp) {
            exp.printStackTrace();
        }
    }
}
