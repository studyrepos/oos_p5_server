package net.scottec.oos5_2.server;

import net.scottec.oos5_2.common.BenutzerVerwaltungAdmin;

public class Server {

    private BenutzerVerwaltungAdmin benutzerVerwaltungAdmin;

    public Server() {
        this.benutzerVerwaltungAdmin = new BenutzerVerwaltungAdmin("usersRemote.s", false);
//      start ServerOrb as new thread
        Thread serverOrbThread = new Thread(new ServerOrb(this.benutzerVerwaltungAdmin));
        serverOrbThread.setDaemon(true);
        serverOrbThread.start();
    }

    public static void main(String... args) {
        new Server();
    }

}
