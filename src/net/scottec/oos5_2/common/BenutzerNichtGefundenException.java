package net.scottec.oos5_2.common;

/**
 * Exception
 * Vergleichbar mit NullPointerException.
 * Geworfen, falls Benutzer nicht in Datenhaltung gefunden.
 */
public class BenutzerNichtGefundenException extends Exception {
    public BenutzerNichtGefundenException(String message) {
        super(message);
    }
}
