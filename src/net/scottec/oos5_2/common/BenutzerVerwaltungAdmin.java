package net.scottec.oos5_2.common;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Eine sehr einfache, nicht persistende Datenverwaltung.
 * Speichert Benutzer in ArrayList<>.
 */
public class BenutzerVerwaltungAdmin implements BenutzerVerwaltung {

    public BenutzerVerwaltungAdmin(String _fileName, boolean _vorhandeneUeberschreiben){
        this.fileName = _fileName;
        if (_vorhandeneUeberschreiben) {
            this.dbInitialisieren();
        }
        else {
            this.dbInitialisierenUndDateiErhalten();
        }
    }

    /**
     * Versucht einen Benutzer in das System aufzunehmen.
     * Existiert der Benutzer bereits, wird eine Exception geworfen.
     * Ist die Aufnahme erfolgreich, terminiert die Funktion ohne Meldung.
     * @param _benutzer : Der aufzunehmende Benutzer
     * @throws BenutzerDoppeltException  : Exception, falls Benutzer bereits vorhanden
     */
    public void benutzerEintragen(Benutzer _benutzer) throws BenutzerDoppeltException {
        this.dbDeserialize();
        if (!this.benutzerList.contains(_benutzer)) {
            this.benutzerList.add(_benutzer);
            this.dbSerialize();
        }
        else {
            throw new BenutzerDoppeltException("Benutzer bereits vorhanden");
        }
    }

    /**
     * Prüft, ob Benutzer in Datenhaltung vorhanden
     * @param _benutzer : Zu überprüfender Benutzer
     * @return  True    : Benutzer gefunden
     *          False   : Benutzer nicht gefunden
     */
    public boolean benutzerOk(Benutzer _benutzer) {
        this.dbDeserialize();
        return this.benutzerList.contains(_benutzer);
    }

    /**
     * Versucht einen Benutzer aus der Datenhaltung zu löschen
     * Terminiert still, falls Löschen erfolgreich, andernfalls wird
     * eine Exception geworfen
     * @param _benutzer : Zu löschender Benutzer
     * @throws BenutzerNichtGefundenException   : Benutzer nicht Gefunden
     */
    public void benutzerLoeschen(Benutzer _benutzer) throws BenutzerNichtGefundenException {
        this.dbDeserialize();
        if (!this.benutzerList.remove(_benutzer)){
            throw new BenutzerNichtGefundenException("Benutzer nicht gefunden");
        }
        else {
            this.dbSerialize();
        }
    }

    public void printBenutzerList() {
        this.dbDeserialize();
        System.out.println("Aktuelle Benutzerliste:");
        for(Benutzer benutzer : benutzerList) {
            System.out.println("- " + benutzer.toString());
        }
        System.out.println("Benutzer gesamt: " + this.benutzerList.size() + "\n");
    }

    // private Attribute
    private List<Benutzer> benutzerList = new ArrayList<>();
    private String fileName;


    /**
     * Initialisiert die Datenhaltung auf Dateibasis.
     */
    private void dbInitialisieren() {
        try {
            FileOutputStream fos = new FileOutputStream(this.fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(new ArrayList<Benutzer>());
            oos.close();
            fos.close();
        }
        catch (FileNotFoundException exp) {
            System.out.println("Datei nicht gefunden");
        }
        catch (IOException exp) {
            exp.printStackTrace();
        }
    }


    /**
     * Initialisiert die Datenhaltung auf Dateibasis.
     */
    private void dbInitialisierenUndDateiErhalten() {
        try {
            File f = new File(this.fileName);
            if (!f.isFile()) {
                System.out.print("Datei nicht vorhanden... Anlegen... ");
                FileOutputStream fos = new FileOutputStream(f);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(new ArrayList<Benutzer>());
                oos.close();
                fos.close();
            }
        }
        catch (FileNotFoundException exp) {
            System.out.println("Datei nicht gefunden");
        }
        catch (IOException exp) {
            exp.printStackTrace();
        }
    }

    private void dbDrop() {
    }

    /**
     * Serialisiert die aktuelle Datenstruktur in die durch fileName
     * definierte Datei
     */
    private void dbSerialize() {
        try {
            FileOutputStream fos = new FileOutputStream(this.fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this.benutzerList);
            oos.close();
            fos.close();
        }
        catch (FileNotFoundException exp) {
            System.out.println("Datei nicht gefunden");
        }
        catch (IOException exp) {
            exp.printStackTrace();
        }
    }

    /**
     * Deserialisiert die Datenstruktur aus der durch fileName
     * definierten Datei
     */
    private void dbDeserialize() {
        try {
            FileInputStream fis = new FileInputStream(this.fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object inObj = ois.readObject();
            // Dieser cast ist nicht sicher und scheitert, falls der Input
            // keine Liste darstellt, oder auch eine Liste mit falschen
            // Objecten
            // -> Type Erasure of Generics
            // Lösbar nur über vererbte ArrayListErweiterung, welche Typ explizit
            // speichert, oder per Reflection zur TypRückgewinnung
            this.benutzerList = (ArrayList<Benutzer>) inObj;
            ois.close();
            fis.close();
        }
        catch (FileNotFoundException exp) {
            System.out.println("Datei nicht gefunden");
        }
        catch (IOException | ClassNotFoundException exp) {
            exp.printStackTrace();
        }
    }



}
